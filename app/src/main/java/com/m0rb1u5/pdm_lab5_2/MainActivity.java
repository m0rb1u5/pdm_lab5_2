package com.m0rb1u5.pdm_lab5_2;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

public class MainActivity extends AppCompatActivity {
   private Button button, button2;
   private ImageButton imageButton;
   private EditText editText;
   private CheckBox checkBox;
   private RadioGroup radioGroup;
   private RadioButton radioButton, radioButton2;
   private ToggleButton toggleButton;
   private RatingBar ratingBar;

   @Override
   protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      setContentView(R.layout.activity_main);
      setUpViews();
   }

   @Override
   public boolean onCreateOptionsMenu(Menu menu) {
      // Inflate the menu; this adds items to the action bar if it is present.
      getMenuInflater().inflate(R.menu.menu_main, menu);
      return true;
   }

   @Override
   public boolean onOptionsItemSelected(MenuItem item) {
      // Handle action bar item clicks here. The action bar will
      // automatically handle clicks on the Home/Up button, so long
      // as you specify a parent activity in AndroidManifest.xml.
      int id = item.getItemId();

      //noinspection SimplifiableIfStatement
      if (id == R.id.action_settings) {
         return true;
      }

      return super.onOptionsItemSelected(item);
   }

   private void setUpViews() {
      button = (Button) findViewById(R.id.button);
      button2 = (Button) findViewById(R.id.button2);
      imageButton = (ImageButton) findViewById(R.id.imageButton);
      editText = (EditText) findViewById(R.id.editText);
      checkBox = (CheckBox) findViewById(R.id.checkBox);
      radioGroup = (RadioGroup) findViewById(R.id.radioGroup);
      radioButton = (RadioButton) findViewById(R.id.radioButton);
      radioButton2 = (RadioButton) findViewById(R.id.radioButton2);
      toggleButton = (ToggleButton) findViewById(R.id.toggleButton);
      ratingBar = (RatingBar) findViewById(R.id.ratingBar);

      button.setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View v) {
            finish();
         }
      });

      View.OnClickListener list = new View.OnClickListener() {
         @Override
         public void onClick(View view) {
            String opcion = "";
            switch (view.getId()) {
               case R.id.radioButton:
                  opcion = "Red";
                  break;
               case R.id.radioButton2:
                  opcion = "Blue";
                  break;
            }
            Toast.makeText(MainActivity.this, opcion, Toast.LENGTH_SHORT).show();
         }
      };
      radioButton.setOnClickListener(list);
      radioButton2.setOnClickListener(list);

      toggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
         @Override
         public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            Toast.makeText(MainActivity.this, buttonView.getText(), Toast.LENGTH_SHORT).show();
         }
      });
   }

   public void sendClick(View view) {
      String allText = new String("campo:" + editText.getText());
      allText += ":checkbox:";
      if(checkBox.isChecked()) {
         allText += "Checked:";
      }
      else {
         allText += "Not Checked:";
      }

      allText += "toggle:";
      if(toggleButton.isChecked()) {
         allText += "Checked:";
      }
      else {
         allText += "Not Checked:";
      }

      allText += "radios:rojo:";
      String redtext = "";
      if(radioButton.isChecked()) {
         redtext = "pulsado:";
      }
      else {
         redtext = "no pulsado:";
      }
      allText += redtext;

      allText += "azul";
      String bluetext = "";
      if(radioButton2.isChecked()) {
         bluetext = "pulsado:";
      }
      else {
         bluetext = "no pulsado:";
      }
      allText += bluetext;

      allText += "ratings:";
      float f = ratingBar.getRating();
      allText += Float.toString(f) + ":";

      Log.d("app", allText);
      Toast.makeText(this, allText, Toast.LENGTH_LONG).show();
   }

   public void checkBoxClick(View view) {
      String text = "";
      if(checkBox.isChecked()) {
         text = "Selected";
         button2.setEnabled(true);
         Toast.makeText(this, "Ya puedes Salvar",Toast.LENGTH_LONG).show();
      }
      else {
         text = "Not selected";
         button2.setEnabled(false);
         Toast.makeText(this, "Hasta que no marques la casilla no podrás salvar",Toast.LENGTH_LONG).show();
      }
      Toast.makeText(this,text,Toast.LENGTH_SHORT).show();
   }
}
